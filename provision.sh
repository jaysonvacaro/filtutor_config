
sudo apt-get install -y nginx;
sudo apt-get install -y mysql-server-5.6;
sudo add-apt-repository ppa:ondrej/php;
sudo apt-get update;
sudo apt-get install -y php-fpm php-mysql php-mbstring php-mcrypt php-xml php-curl;

sudo cp php.ini /etc/php/7.0/fpm/php.ini;

sudo cp www.conf /etc/php/7.0/fpm/pool.d/www.conf;

sudo cp nginx_config /etc/nginx/sites-available/default;
# change the server_name to ServerNameOrIpAddress

sudo service nginx restart && sudo service php7.0-fpm restart;

cd ~/;

curl -sS https://getcomposer.org/installer | php;
sudo mv composer.phar /usr/local/bin/composer;

sudo apt-get install -y npm;
npm install -g bower;

sudo ln -s /usr/bin/nodejs /usr/bin/node;
mkdir /var/www;

# ext-dom extension


# mkdir /var/www/filipinotutor/storage/app/materials;
# mkdir /var/www/filipinotutor/storage/app/avatars;
# sudo chmod -R 775 /var/www/filipinotutor;
# sudo chmod -R 777 /var/www/filipinotutor/storage; 
# after composer install
